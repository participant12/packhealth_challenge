# JavaScript Lambda Function Challenge

## Overview

In index.js, make the handler function return an array of the glossary items from healthcare.gov (the url is specified in index.js) conditionally filtered based on the following criteria:

- If a language is specified as an argument, only return items for that language.
- If a language is not specified, return all items.

## Directions

1. Clone this repository.
2. Checkout a new branch.
3. `npm install`
4. Make any changes you need to meet the requirements.
5. Add _unit_ test implementations in test/indexSpec.js and ensure the code is 100% covered. (Hint: You will need to use Sinon to mock the healthcare.gov response.)
6. Run `npm test` to make sure the tests pass.
7. Reply to the email we sent you with a link to your solution in your Bitbucket repo.  Do NOT create a pull request to our repo.
